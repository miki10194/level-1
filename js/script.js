$(document).ready(function() {
	$("#input-calendar").datepicker("disable");
});

$("#input-calendar").datepicker({
	buttonImage: 'images/calendar.png',
	buttonImageOnly: true,
	buttonText: 'Open calendar',
	changeMonth: true,
	changeYear: true,
	showOn: 'both',
	onSelect: function(dateText) { 
		var date = new Date(dateText);
		var day = date.getDate();
		var month = date.getMonth()+1;
		var year = date.getFullYear();
		year = year.toString().substr(2, 2);
		$("#input-day").val(day);
		$("#input-month").val(month);
		$("#input-year").val(year);
	} 
});

$("#input-radio-now, #input-radio-choose").click(function(){
	var date = new Date();
	var day = date.getDate();
	var month = date.getMonth()+1;
	var year = date.getFullYear();
	year = year.toString().substr(2, 2);
	$("#input-day").val(day);
	$("#input-month").val(month);
	$("#input-year").val(year);
	if($("#input-radio-now").is(":checked")) {
		$("#input-calendar").datepicker("disable");
	} else if($("#input-radio-choose").is(":checked")) {
		$("#input-calendar").datepicker("enable");

	} 	
});

$("#select-voucher").change(function(){
    var y = $("#select-voucher option:selected").val();
	if(y == "£100") {
		alert(y + " This voucher is not available.")
		$(this).val("£50");
	}	
});

function validateEmail(email) {
  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
}

$("#formularz").submit(function(event) {

	var labelFrom = $(".label-from").text();
	var labelEmail = $(".label-email").text();
	var labelTo = $(".label-to").text();
	var labelRecipient = $(".label-recipient").text();
	var labelVoucher = $(".label-voucher").text();
	var labelMessage = $(".label-message").text();

	var from = $("#input-from").val();
	var email = $("#input-email").val();
	var to = $("#input-to").val();
	var recipient = $("#input-recipient").val();
	var voucher = $("#select-voucher").val();
	var day = $("#input-day").val();
	var month = $("#input-month").val();
	var year = $("#input-year").val();
	var message = $("#textarea-message").val();
	var radioNow = $("#input-radio-now");
	var radioChoose = $("#input-radio-choose");

	var placeFrom = $("#input-from").attr("placeholder");
	var placeEmail = $("#input-email").attr("placeholder");
	var placeTo = $("#input-to").attr("placeholder");
	var placeRecipient = $("#input-recipient").attr("placeholder");

	if(	from!="" && from!=placeFrom && 
		email!="" && email!=placeEmail && 
		to!="" && to!=placeTo && 
		recipient!="" && recipient!=placeRecipient 
		&& radioNow.prop("checked") || radioChoose.prop("checked")) {

		if (!validateEmail(email)) {
			alert("Your email is not valid");
			event.preventDefault();
		} else if (!validateEmail(recipient)) {
			alert("Recipient email is not valid");
			event.preventDefault();
		} else {
			alert(labelFrom 		+ " " + from 			+ "\n" 
				+ labelEmail 		+ " " + email 			+ "\n" 
				+ labelTo 			+ " " + to 				+ "\n" 
				+ labelRecipient 	+ " " + recipient		+ "\n"
				+ labelVoucher 		+ " " + voucher 		+ "\n"
				+ "Date " + day + "-" + month + "-" + year 	+ "\n"
				+ labelMessage 		+ " " + message);
			$("#formularz")[0].reset();
		}
	} else {
		alert("Please, fill the form");
		event.preventDefault(); 
	}

});






